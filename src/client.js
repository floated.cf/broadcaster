import WebSocket from 'ws'

const ws = new WebSocket('ws://localhost:8099')

ws.on('open', () => {
  console.log('Connected!')
})

ws.on('message', message => {
  console.log(message)
  ws.close()
})
