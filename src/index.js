import WebSocket from 'ws'
import fs from 'fs'

import readfile from './aux/readfile'
import broadcast from './aux/broadcast'

const wss = new WebSocket.Server({port: 8099})

const files = {
  'docker': 'dockerps',
  'nginx': 'sites'
}

let status = {
  'docker': readfile(files.docker),
  'nginx': readfile(files.nginx)
}

wss.on('connection', ws => {
  // Log hello
  console.log('😍 Someone connected')
  // Log bye
  ws.on('close', () => {
    console.log('😭 Someone left')
  })
  // What to do
  console.log('📤 Sending data!')
  ws.send(JSON.stringify(status))
})

Object.keys(files).forEach(key => {
  console.log(`👓 Watching ${files[key]}`)
  fs.watchFile(`./status/${files[key]}`, (curr, prev) => {
    console.log(`😎 New update @ ${files[key]}`)
    status[key] = readfile(files[key])
    broadcast(wss, status)
  })
})


