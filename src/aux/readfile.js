import fs from 'fs'
import parse from './output'

export default file => JSON.stringify(parse[file](fs.readFileSync('./status/' + file, 'utf8')))
