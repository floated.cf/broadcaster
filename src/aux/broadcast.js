import WebSocket from 'ws'

export default (wss, status) => {
  wss.clients.forEach(client => {
    if (client.readyState === WebSocket.OPEN) {
      client.send(JSON.stringify(status))
    }
  })
}
