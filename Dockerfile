FROM mhart/alpine-node:9

RUN mkdir -p /code
WORKDIR code
ADD . /code

RUN npm install

CMD npm run start
EXPOSE 8099
